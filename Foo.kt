import java.security.SecureRandom

class Foo {

    fun whenRandomNumberWithJavaSecureRandom_thenResultsInGivenRanges() {
        val secureRandom = SecureRandom()
		secureRandom.setSeed(123456L) // Noncompliant
        secureRandom.nextInt(100)
        assertTrue { randomLong >= 0 }
        assertTrue { randomLong < 100 }
    }

    fun whenRandomNumberWithJavaSecureRandoms() {
        val secureRandom = SecureRandom(10)
        secureRandom.nextInt(100)
        assertTrue { randomLong >= 0 }
        assertTrue { randomLong < 100 }
    }
}